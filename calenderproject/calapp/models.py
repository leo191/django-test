from django.db import models

class Entry(models.Model):
	name = models.CharField(max_length=100)
	date = models.DateTimeField()
	description = models.TextField()
	created = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return f'{self.id} {self.name} {self.date}'

class Hosts(models.Model):
	name = models.CharField(max_length=100)
	ip = models.GenericIPAddressField(protocol='both')


	def __str__(self):
		return self.name


class Some(models.Model):
	name = models.CharField(max_length=100)
	sname = models.CharField(max_length=100)

	def __str__(self):
		return self.name



class csv(models.Model):
	outputversion = models.IntegerField()
	file = models.CharField(max_length=200)
	cpe = models.CharField(max_length=200)
	cve =models.CharField(max_length=200)
	cvss =models.FloatField(default=0.0)
	match_type = models.IntegerField()
	hostname = models.CharField(max_length=200)
	user_key = models.CharField(max_length=200)

	def __str__(self):
		return self.hostname
