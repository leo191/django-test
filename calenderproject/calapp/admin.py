from django.contrib import admin
from .models import Entry, Hosts, Some

admin.site.register(Entry)
admin.site.register(Hosts)
admin.site.register(Some)
# Register your models here.
