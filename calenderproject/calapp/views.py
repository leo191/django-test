from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .models import Entry, Hosts
from .forms import EntryForm
import pandas as pd
import json
import requests as req
import logging





def sign(request):
	return render(request, 'calapp/login_signup.html')

def index(request):
	entries = Entry.objects.all()
	return render(request, 'calapp/index.html', {'entries':entries})
def details(request,pk):

	return HttpResponse('Hello')

def add(request):

	if request.method == 'POST':
		form  = EntryForm(request.POST)
		if form.is_valid():
			name = form.cleaned_data['name']
			date = form.cleaned_data['date']
			description = form.cleaned_data['description']
			return HttpResponseRedirect('/')
	else:
		form = EntryForm()

	return render(request, 'calapp/form.html', {'form': form})



@csrf_exempt
def home(request):
	allhosts = Hosts.objects.all()
	if request.method == 'POST':
		#jsonres = json.loads(request.POST)

		host = Hosts()
		job = json.loads(request.body)
		host.name = job['host']
		host.ip = job['ip']
		host.save()
		print(job['host'])
		# print(json_data)
		allhosts = Hosts.objects.all()
	return render(request, 'calapp/home.html', {'allhosts':allhosts})


def loading(request):

	return render(request, 'calapp/load.html')



def showcsv(request):
	# df=pd.read_csv("/home/leo/PycharmProjects/CalenderApp/calenderproject/calapp/templates/calapp/myreport.csv")
	# df.to_html(classes=["table-bordered", "table-striped", "table-hover"],
	# 	'/home/leo/PycharmProjects/CalenderApp/calenderproject/calapp/templates/calapp/gentable.html')
	return render(request, 'calapp/tabled.html')
